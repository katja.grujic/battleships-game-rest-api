package grujic.battleships.web.converter;

import grujic.battleships.game.Location;
import grujic.battleships.game.Settings;
import grujic.battleships.web.domain.form.SalvoForm;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class SalvoToLocations implements Converter<SalvoForm, List<Location>> {

    @Override
    public List<Location> convert(SalvoForm source) {
        return source.getSalvo()
                .stream()
                .map(this::mapStringToLocation)
                .collect(Collectors.toList());
    }

    private Location mapStringToLocation(String shot) {
        String[] coordinates = shot.split("x");
        if (coordinates.length != 2) {
            throw new IllegalArgumentException();
        }

        int row = Integer.parseInt(coordinates[0]) - 1;
        int column = (int) coordinates[1].charAt(0) - 'A';
        if ( row > Settings.BOARD_SIZE || column > Settings.BOARD_SIZE || coordinates[1].length() != 1) {
            throw new IllegalArgumentException();
        }

        return new Location(row, column);
    }
}
