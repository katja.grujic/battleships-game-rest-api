package grujic.battleships.web.converter;

import grujic.battleships.game.Location;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class LocationToSalvo implements Converter<Location, String> {

    @Override
    public String convert(Location source) {
        StringBuilder sb = new StringBuilder();
        sb.append(String.valueOf(source.getRow() + 1));
        sb.append("x");
        sb.append((char)(source.getColumn() + 'A'));

        return sb.toString();
    }
}
