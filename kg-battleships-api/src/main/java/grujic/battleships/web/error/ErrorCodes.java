package grujic.battleships.web.error;

public abstract class ErrorCodes {
    public static final String USERNAME_ALREADY_TAKEN = "error.username-already-taken";
    public static final String PLAYER_NOT_FOUND = "error.unknown-user-id";
    public static final String GAME_NOT_FOUND = "error.unknown-game-id";
    public static final String PLAYER_NOT_IN_GAME = "error.player-not-in-game";

    private ErrorCodes() {
    }
}
