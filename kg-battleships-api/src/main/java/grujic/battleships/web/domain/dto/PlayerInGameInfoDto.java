package grujic.battleships.web.domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PlayerInGameInfoDto {

    @JsonProperty("player_id")
    private Long playerId;

    private String[] board;

    @JsonProperty("remaining_ships")
    private int remainingShips;

    private boolean autopilot;

    public PlayerInGameInfoDto(Long playerId, String[] board, int remainingShips, boolean autopilot) {
        this.playerId = playerId;
        this.board = board;
        this.remainingShips = remainingShips;
        this.autopilot = autopilot;
    }

    public Long getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Long playerId) {
        this.playerId = playerId;
    }

    public String[] getBoard() {
        return board;
    }

    public void setBoard(String[] board) {
        this.board = board;
    }

    public int getRemainingShips() {
        return remainingShips;
    }

    public void setRemainingShips(int remainingShips) {
        this.remainingShips = remainingShips;
    }

    public boolean isAutopilot() {
        return autopilot;
    }

    public void setAutopilot(boolean autopilot) {
        this.autopilot = autopilot;
    }
}
