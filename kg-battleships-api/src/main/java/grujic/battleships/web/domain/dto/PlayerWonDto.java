package grujic.battleships.web.domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PlayerWonDto extends GameOverDto {

    @JsonProperty("won")
    private Long id;

    public PlayerWonDto(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
