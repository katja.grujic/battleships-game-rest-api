package grujic.battleships.web.domain.dto;

import grujic.battleships.game.GameStatus;
import com.fasterxml.jackson.annotation.JsonProperty;

public class GameInvolvedDto {

    @JsonProperty("game_id")
    private Long gameId;

    @JsonProperty("opponent_id")
    private Long opponentId;

    private GameStatus status;

    public Long getGameId() {
        return gameId;
    }

    public void setGameId(Long gameId) {
        this.gameId = gameId;
    }

    public Long getOpponentId() {
        return opponentId;
    }

    public void setOpponentId(Long opponentId) {
        this.opponentId = opponentId;
    }

    public GameStatus getStatus() {
        return status;
    }

    public void setStatus(GameStatus status) {
        this.status = status;
    }
}
