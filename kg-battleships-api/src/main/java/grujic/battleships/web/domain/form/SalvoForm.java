package grujic.battleships.web.domain.form;

import java.util.List;

public class SalvoForm {

    private List<String> salvo;

    public SalvoForm() {
    }

    public SalvoForm(List<String> salvo) {
        this.salvo = salvo;
    }

    public List<String> getSalvo() {
        return salvo;
    }

    public void setSalvo(List<String> salvo) {
        this.salvo = salvo;
    }
}
