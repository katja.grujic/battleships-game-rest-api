package grujic.battleships.web.domain.dto;

import grujic.battleships.game.ShotStatus;

import java.util.Map;

public class SalvoShotsDto {

    private Map<String, ShotStatus> salvo;

    private GameOverDto game;

    public SalvoShotsDto(Map<String, ShotStatus> salvo, GameOverDto game) {
        this.salvo = salvo;
        this.game = game;
    }

    public Map<String, ShotStatus> getSalvo() {
        return salvo;
    }

    public void setSalvo(Map<String, ShotStatus> salvo) {
        this.salvo = salvo;
    }

    public GameOverDto getGame() {
        return game;
    }

    public void setGame(GameOverDto game) {
        this.game = game;
    }
}
