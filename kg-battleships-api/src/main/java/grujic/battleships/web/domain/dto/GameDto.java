package grujic.battleships.web.domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GameDto {

    @JsonProperty("player_id")
    private Long challengerId;

    @JsonProperty("opponent_id")
    private Long opponentId;

    @JsonProperty("game_id")
    private Long gameId;

    @JsonProperty("starting")
    private Long startingPlayerId;

    public Long getChallengerId() {
        return challengerId;
    }

    public void setChallengerId(Long challengerId) {
        this.challengerId = challengerId;
    }

    public Long getOpponentId() {
        return opponentId;
    }

    public void setOpponentId(Long opponentId) {
        this.opponentId = opponentId;
    }

    public Long getGameId() {
        return gameId;
    }

    public void setGameId(Long gameId) {
        this.gameId = gameId;
    }

    public Long getStartingPlayerId() {
        return startingPlayerId;
    }

    public void setStartingPlayerId(Long startingPlayerId) {
        this.startingPlayerId = startingPlayerId;
    }
}
