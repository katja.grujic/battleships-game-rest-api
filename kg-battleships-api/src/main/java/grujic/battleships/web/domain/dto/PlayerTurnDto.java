package grujic.battleships.web.domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PlayerTurnDto extends GameOverDto {

    @JsonProperty("player_turn")
    private Long id;

    public PlayerTurnDto(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

}
