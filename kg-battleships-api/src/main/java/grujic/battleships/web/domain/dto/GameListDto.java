package grujic.battleships.web.domain.dto;

import java.util.List;

public class GameListDto {

    private List<GameInvolvedDto> games;

    public GameListDto(List<GameInvolvedDto> games) {
        this.games = games;
    }

    public List<GameInvolvedDto> getGames() {
        return games;
    }

    public void setGames(List<GameInvolvedDto> games) {
        this.games = games;
    }
}
