package grujic.battleships.web.domain.dto;

import java.util.List;

public class PlayerListDto {

    private List<PlayerDto> players;

    public PlayerListDto(List<PlayerDto> players) {
        this.players = players;
    }

    public List<PlayerDto> getPlayers() {
        return players;
    }

    public void setPlayers(List<PlayerDto> players) {
        this.players = players;
    }
}
