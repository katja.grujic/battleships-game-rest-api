package grujic.battleships.web.domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AppErrorDto {

    @JsonProperty("error-code")
    private String code;

    @JsonProperty("error-arg")
    private Object arg;

    public AppErrorDto() {
    }

    public AppErrorDto(String code, Object arg) {
        this.code = code;
        this.arg = arg;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Object getArg() {
        return arg;
    }

    public void setArg(Object arg) {
        this.arg = arg;
    }
}
