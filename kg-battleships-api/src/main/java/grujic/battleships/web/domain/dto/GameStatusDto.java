package grujic.battleships.web.domain.dto;


public class GameStatusDto {

    private PlayerInGameInfoDto self;

    private PlayerInGameInfoDto opponent;

    private GameOverDto game;

    public GameStatusDto(PlayerInGameInfoDto self, PlayerInGameInfoDto opponent, GameOverDto game) {
        this.self = self;
        this.opponent = opponent;
        this.game = game;
    }

    public PlayerInGameInfoDto getSelf() {
        return self;
    }

    public void setSelf(PlayerInGameInfoDto self) {
        this.self = self;
    }

    public PlayerInGameInfoDto getOpponent() {
        return opponent;
    }

    public void setOpponent(PlayerInGameInfoDto opponent) {
        this.opponent = opponent;
    }

    public GameOverDto getGame() {
        return game;
    }

    public void setGame(GameOverDto game) {
        this.game = game;
    }
}
