package grujic.battleships.web.domain.form;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CreateGameForm {

    @JsonProperty("player_id")
    private Long challengerId;

    public Long getChallengerId() {
        return challengerId;
    }

    public void setChallengerId(Long challengerId) {
        this.challengerId = challengerId;
    }
}
