package grujic.battleships.web.service.impl;

import grujic.battleships.game.Location;
import grujic.battleships.game.Settings;
import grujic.battleships.model.PlayerInGameInfo;
import grujic.battleships.web.controller.PlayerController;
import grujic.battleships.web.converter.LocationToSalvo;
import grujic.battleships.web.domain.form.SalvoForm;
import grujic.battleships.web.service.AutopilotService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Service
public class AutopilotServiceImpl implements AutopilotService {

    @Autowired
    private PlayerController playerController;

    @Autowired
    private LocationToSalvo locationToSalvo;

    static Random rnd = new Random(System.currentTimeMillis());

    @Override
    @Async
    public void playTurn(Long gameId, PlayerInGameInfo playerInfo) {
        List<String> salvo = generateSalvo(playerInfo);

        SalvoForm salvoForm = new SalvoForm(salvo);
        playerController.handleShots(salvoForm, playerInfo.getPlayer().getId(), gameId);
    }

    private List<String> generateSalvo(PlayerInGameInfo playerInfo) {
        List<String> locations = new ArrayList<>();
        for (int i = 0; i < playerInfo.getSelfBoard().getAliveShips(); i++) {
            locations.add(locationToSalvo.convert(getRandomLocation()));
        }
        for (String w : locations) {
            System.out.println(w);
        }
        return locations;
    }

    private static Location getRandomLocation(){
        return new Location(rnd.nextInt(Settings.BOARD_SIZE), rnd.nextInt(Settings.BOARD_SIZE));
    }


}
