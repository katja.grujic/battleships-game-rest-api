package grujic.battleships.web.service;

import grujic.battleships.model.PlayerInGameInfo;

public interface AutopilotService {

    void playTurn(Long gameId, PlayerInGameInfo playerInfo);
}
