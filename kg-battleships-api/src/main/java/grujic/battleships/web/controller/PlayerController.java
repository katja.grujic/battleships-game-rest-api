package grujic.battleships.web.controller;

import grujic.battleships.exception.EmailAlreadyExistsException;
import grujic.battleships.game.GameBoard;
import grujic.battleships.game.Location;
import grujic.battleships.game.Settings;
import grujic.battleships.game.ShotStatus;
import grujic.battleships.mapper.BoardMapper;
import grujic.battleships.model.Board;
import grujic.battleships.model.Game;
import grujic.battleships.model.Player;
import grujic.battleships.model.PlayerInGameInfo;
import grujic.battleships.service.BoardService;
import grujic.battleships.service.GameService;
import grujic.battleships.service.PlayerInGameInfoService;
import grujic.battleships.web.converter.LocationToSalvo;
import grujic.battleships.web.converter.SalvoToLocations;
import grujic.battleships.web.domain.dto.*;
import grujic.battleships.web.domain.form.CreateGameForm;
import grujic.battleships.web.domain.form.PlayerForm;
import grujic.battleships.web.domain.form.SalvoForm;
import grujic.battleships.web.mapper.dto.GameDtoMapper;
import grujic.battleships.web.mapper.dto.GameInvolvedDtoMapper;
import grujic.battleships.web.mapper.dto.PlayerDtoMapper;
import grujic.battleships.web.mapper.form.PlayerFormMapper;
import grujic.battleships.web.service.AutopilotService;
import grujic.battleships.service.PlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/player")
public class PlayerController {

    private PlayerService playerService;

    private GameService gameService;

    private BoardService boardService;

    private PlayerInGameInfoService playerInGameInfoService;

    private AutopilotService autopilotService;

    private PlayerFormMapper playerFormMapper;

    private PlayerDtoMapper playerDtoMapper;

    private GameDtoMapper gameDtoMapper;

    private BoardMapper boardMapper;

    private GameInvolvedDtoMapper gameInvolvedDtoMapper;

    private SalvoToLocations salvoToLocations;

    private LocationToSalvo locationToSalvo;

    @Autowired
    public PlayerController(PlayerService playerService,
                            GameService gameService,
                            BoardService boardService,
                            PlayerInGameInfoService playerInGameInfoService,
                            @Lazy AutopilotService autopilotService,
                            PlayerFormMapper playerFormMapper,
                            PlayerDtoMapper playerDtoMapper,
                            GameDtoMapper gameDtoMapper,
                            BoardMapper boardMapper,
                            GameInvolvedDtoMapper gameInvolvedDtoMapper,
                            SalvoToLocations salvoToLocations,
                            LocationToSalvo locationToSalvo) {
        this.playerService = playerService;
        this.gameService = gameService;
        this.boardService = boardService;
        this.playerInGameInfoService = playerInGameInfoService;
        this.autopilotService = autopilotService;
        this.playerFormMapper = playerFormMapper;
        this.playerDtoMapper = playerDtoMapper;
        this.gameDtoMapper = gameDtoMapper;
        this.boardMapper = boardMapper;
        this.gameInvolvedDtoMapper = gameInvolvedDtoMapper;
        this.salvoToLocations = salvoToLocations;
        this.locationToSalvo = locationToSalvo;
    }

    @PostMapping
    public ResponseEntity<Void> createPlayer(@RequestBody PlayerForm playerForm){
        if (playerService.existsByEmail(playerForm.getEmail())) {
            throw new EmailAlreadyExistsException(playerForm.getEmail());
        }

        Player player = playerFormMapper.mapToEntity(playerForm);
        playerService.save(player);

        return ResponseEntity.created(URI.create("/player/" + player.getId())).build();

    }

    @GetMapping("/{id}")
    public ResponseEntity<PlayerDto> getPlayer(@PathVariable Long id) {
        if (!playerService.existsById(id)) {
            return ResponseEntity.notFound().build();
        }

        Player player = playerService.getById(id);
        return ResponseEntity.ok(playerDtoMapper.mapFromEntity(player));

    }

    @GetMapping("/list")
    public ResponseEntity<PlayerListDto> getAllPlayers() {
        List<PlayerDto> players = playerService.getAll()
                .stream()
                .map(playerDtoMapper::mapFromEntity)
                .collect(Collectors.toList());

        return ResponseEntity.ok(new PlayerListDto(players));

    }

    @PostMapping("/{opponentId}/game")
    public ResponseEntity<GameDto> createGame(@RequestBody CreateGameForm createGameForm,
                                              @PathVariable Long opponentId) {

        Player challenger = playerService.getById(createGameForm.getChallengerId());
        Player opponent = playerService.getById(opponentId);

        PlayerInGameInfo challengerInGameInfo = createPlayerInGameInfo(challenger);
        PlayerInGameInfo opponentInGameInfo = createPlayerInGameInfo(opponent);

        Game game = new Game(challengerInGameInfo, opponentInGameInfo);
        game.setPlayerTurn(new Random().nextBoolean() ? challenger : opponent);
        game = gameService.save(game);

        return ResponseEntity.created(URI.create("/game/" + game.getId())).body(gameDtoMapper.mapFromEntity(game));
    }

    @GetMapping("/{playerId}/game/{gameId}")
    public ResponseEntity<GameStatusDto> getGameStatus(@PathVariable Long playerId,
                                                       @PathVariable Long gameId) {

        Player player = playerService.getById(playerId);
        Game game = gameService.getById(gameId);

        PlayerInGameInfo selfInfo = playerInGameInfoService.getSelfInfo(game, player);
        PlayerInGameInfo opponentInfo = playerInGameInfoService.getOpponentInfo(game, player);

        GameBoard selfBoard = boardMapper.mapFromEntity(selfInfo.getSelfBoard());
        GameBoard opponentBoard = boardMapper.mapFromEntity(opponentInfo.getSelfBoard());

        PlayerInGameInfoDto selfInfoDto = new PlayerInGameInfoDto(playerId,
                                                                selfBoard.toStringSelfView(),
                                                                selfInfo.getSelfBoard().getAliveShips(),
                                                                selfInfo.isAutopilot());
        PlayerInGameInfoDto opponentInfoDto = new PlayerInGameInfoDto(opponentInfo.getPlayer().getId(),
                                                                        opponentBoard.toStringOpponentView(),
                                                                        opponentInfo.getSelfBoard().getAliveShips(),
                                                                        opponentInfo.isAutopilot());

        if (game.getWon() == null) {
            return ResponseEntity
                    .ok(new GameStatusDto(selfInfoDto, opponentInfoDto, new PlayerTurnDto(game.getPlayerTurn().getId())));
        }

        return ResponseEntity
                .ok(new GameStatusDto(selfInfoDto, opponentInfoDto, new PlayerWonDto(game.getWon().getId())));
    }

    @GetMapping("/{playerId}/game/list")
    public ResponseEntity<GameListDto> getAllPlayerGames(@PathVariable Long playerId) {
        if (!playerService.existsById(playerId)) {
            return ResponseEntity.notFound().build();
        }

        List<Long> playerInfoIds = playerInGameInfoService.findAllByPlayerId(playerId);

        if (playerInfoIds.isEmpty()) {
            return ResponseEntity.noContent().build();
        }

        List<GameInvolvedDto> gameInvolvedDtos = new ArrayList<>();
        for (Long playerInfoId : playerInfoIds) {
            Game game = gameService.findByPlayerInGameInfo(playerInfoId);
            gameInvolvedDtos.add(gameInvolvedDtoMapper.mapFromEntity(
                    game,
                    playerInGameInfoService.getSelfInfo(game, playerService.getById(playerId))));
        }

        return ResponseEntity.ok(new GameListDto(gameInvolvedDtos));
    }

    @PutMapping("/{playerId}/game/{gameId}")
    public ResponseEntity<SalvoShotsDto> handleShots(@RequestBody SalvoForm salvo,
                                                     @PathVariable Long playerId,
                                                     @PathVariable Long gameId) {

        Player player = playerService.getById(playerId);
        Game game = gameService.getById(gameId);

        if (game.getPlayerTurn().getId() != playerId) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }

        if (game.getWon() != null) {
            return ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED).build();
        }

        PlayerInGameInfo playerInfo = playerInGameInfoService.getSelfInfo(game, player);
        PlayerInGameInfo opponentInfo = playerInGameInfoService.getOpponentInfo(game, player);

        GameBoard playerBoard = boardMapper.mapFromEntity(playerInfo.getSelfBoard());
        GameBoard opponentBoard = boardMapper.mapFromEntity(opponentInfo.getSelfBoard());

        if (salvo.getSalvo().size() != playerBoard.getAliveShips()) {
            return ResponseEntity.badRequest().build();
        }

        List <Location> locations;
        try {
            locations = salvoToLocations.convert(salvo);
        } catch (Exception ex) {
            return ResponseEntity.badRequest().build();
        }

        HashMap<String, ShotStatus> map = new HashMap<>();
        locations.forEach(location -> map.put(locationToSalvo.convert(location), opponentBoard.getShotResult(location)));

        Board oppEntityBoard = boardMapper.mapToEntity(opponentBoard);
        opponentInfo.getSelfBoard().setCells(oppEntityBoard.getCells());
        opponentInfo.getSelfBoard().setAliveShips(oppEntityBoard.getAliveShips());
        playerInGameInfoService.save(opponentInfo);

        if (opponentBoard.getAliveShips() == 0) {
            game.setWon(player);
        } else {
            game.setPlayerTurn(opponentInfo.getPlayer());
        }

        gameService.save(game);

        if (opponentBoard.getAliveShips() == 0) {
            return ResponseEntity.ok(new SalvoShotsDto(map, new PlayerWonDto(playerId)));
        }

        if (opponentInfo.isAutopilot()) {
            autopilotService.playTurn(gameId, opponentInfo);
        }

        return ResponseEntity.ok(new SalvoShotsDto(map, new PlayerTurnDto(opponentInfo.getPlayer().getId())));

    }

    @PutMapping("/{playerId}/game/{gameId}/autopilot")
    public ResponseEntity<Void> turnOnAutopilot(@PathVariable Long playerId,
                                                @PathVariable Long gameId) {

        Player player = playerService.getById(playerId);
        Game game = gameService.getById(gameId);

        if (game.getWon() != null) {
            return ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED).build();
        }

        PlayerInGameInfo playerInfo = playerInGameInfoService.getSelfInfo(game, player);
        playerInfo.setAutopilot(true);
        playerInGameInfoService.save(playerInfo);

        if (game.getPlayerTurn() == player){
            autopilotService.playTurn(gameId, playerInGameInfoService.getSelfInfo(game, player));
        }

        return ResponseEntity.noContent().build();
    }

    // ----------- UTIL METHODS -------------------------------------------------------------------------------


    private PlayerInGameInfo createPlayerInGameInfo(Player player) {
        GameBoard gameBoard = GameBoard.builder().setShips(Settings.getShips()).build();
        System.out.println(gameBoard.toString());
        Board board = boardMapper.mapToEntity(gameBoard);
        boardService.save(board);

        PlayerInGameInfo playerInGameInfo = new PlayerInGameInfo(player, board);
        return playerInGameInfoService.save(playerInGameInfo);
    }

}
