package grujic.battleships.web.controller;

import grujic.battleships.exception.EmailAlreadyExistsException;
import grujic.battleships.exception.GameNotFoundException;
import grujic.battleships.exception.PlayerNotFoundException;
import grujic.battleships.exception.PlayerNotInGameException;
import grujic.battleships.web.domain.dto.AppErrorDto;
import grujic.battleships.web.error.ErrorCodes;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ControllerExceptionHandler {

    @ResponseStatus(value = HttpStatus.CONFLICT)
    @ExceptionHandler(EmailAlreadyExistsException.class)
    @ResponseBody
    public AppErrorDto handleEmailAlreadyExists(Exception ex) {
        EmailAlreadyExistsException emailAlreadyExistsException = (EmailAlreadyExistsException) ex;

        AppErrorDto response = new AppErrorDto();
        response.setCode(ErrorCodes.USERNAME_ALREADY_TAKEN);
        response.setArg(emailAlreadyExistsException.getEmail());
        return response;
    }

    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    @ExceptionHandler(PlayerNotFoundException.class)
    @ResponseBody
    public AppErrorDto handlePlayerNotFound(Exception ex) {
        PlayerNotFoundException playerNotFoundException = (PlayerNotFoundException) ex;

        AppErrorDto response = new AppErrorDto();
        response.setCode(ErrorCodes.PLAYER_NOT_FOUND);
        response.setArg(playerNotFoundException.getPlayerId());
        return response;
    }

    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    @ExceptionHandler(GameNotFoundException.class)
    @ResponseBody
    public AppErrorDto handleGameNotFound(Exception ex) {
        GameNotFoundException gameNotFoundException = (GameNotFoundException) ex;

        AppErrorDto response = new AppErrorDto();
        response.setCode(ErrorCodes.GAME_NOT_FOUND);
        response.setArg(gameNotFoundException.getGameId());
        return response;
    }

    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    @ExceptionHandler(PlayerNotInGameException.class)
    @ResponseBody
    public AppErrorDto handlePlayerNotInGame(Exception ex) {
        PlayerNotInGameException playerNotInGameException = (PlayerNotInGameException) ex;

        AppErrorDto response = new AppErrorDto();
        response.setCode(ErrorCodes.PLAYER_NOT_IN_GAME);
        response.setArg(playerNotInGameException.getGameId());
        return response;
    }

}
