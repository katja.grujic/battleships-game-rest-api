package grujic.battleships.web.mapper.form;

import grujic.battleships.model.Player;
import grujic.battleships.web.domain.form.PlayerForm;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

@Component
public class PlayerFormMapper {
    private static final String PLAYER_FORM_KEY = "playerForm";

    public PlayerForm mapFromEntity(Player entity) {
        return mapFromEntity(entity, null);
    }

    public Player mapToEntity(PlayerForm form) {
        return mapToEntity(form, null);
    }

    public PlayerForm mapFromEntity(Player entity, Locale locale) {
        throw new UnsupportedOperationException();
    }

    public PlayerForm mapFromEntity(Player entity, Locale locale, Map<String, Object> config) {
        throw new UnsupportedOperationException();
    }

    public Player mapToEntity(PlayerForm form, Locale locale) {
        Map<String, Object> config = new HashMap<>();
        config.put(PLAYER_FORM_KEY, new Player());

        return mapToEntity(form, locale, config);
    }

    public Player mapToEntity(PlayerForm form, Locale locale, Map<String, Object> config) {
        Player player = (Player) config.get(PLAYER_FORM_KEY);
        player.setName(form.getName());
        player.setEmail(form.getEmail());

        return player;
    }

}
