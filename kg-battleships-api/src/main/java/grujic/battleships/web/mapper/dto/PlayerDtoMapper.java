package grujic.battleships.web.mapper.dto;

import grujic.battleships.model.Player;
import grujic.battleships.web.domain.dto.PlayerDto;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

@Component
public class PlayerDtoMapper {
    private static final String PLAYER_KEY = "playerDto";

    public PlayerDto mapFromEntity(Player entity) {
        return mapFromEntity(entity, null);
    }

    public Player mapToEntity(PlayerDto dto) {
        return mapToEntity(dto, null);
    }

    public PlayerDto mapFromEntity(Player entity, Locale locale) {
        Map<String, Object> config = new HashMap<>();
        config.put(PLAYER_KEY, new PlayerDto());

        return mapFromEntity(entity, locale, config);
    }

    public PlayerDto mapFromEntity(Player entity, Locale locale, Map<String, Object> config) {
        PlayerDto playerDto = (PlayerDto) config.get(PLAYER_KEY);
        playerDto.setId(entity.getId());
        playerDto.setName(entity.getName());
        playerDto.setEmail(entity.getEmail());

        return playerDto;
    }

    public Player mapToEntity(PlayerDto source, Locale locale) {
        throw new UnsupportedOperationException();
    }

    public Player mapToEntity(PlayerDto source, Locale locale, Map<String, Object> config) {
        throw new UnsupportedOperationException();
    }
}
