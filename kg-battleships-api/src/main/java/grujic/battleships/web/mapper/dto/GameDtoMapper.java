package grujic.battleships.web.mapper.dto;

import grujic.battleships.model.Game;
import grujic.battleships.web.domain.dto.GameDto;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

@Component
public class GameDtoMapper {
    private static final String GAME_KEY = "gameDto";

    public GameDto mapFromEntity(Game entity) {
        return mapFromEntity(entity, null);
    }

    public Game mapToEntity(GameDto dto) {
        return mapToEntity(dto, null);
    }

    public GameDto mapFromEntity(Game entity, Locale locale) {
        Map<String, Object> config = new HashMap<>();
        config.put(GAME_KEY, new GameDto());

        return mapFromEntity(entity, locale, config);
    }

    public GameDto mapFromEntity(Game entity, Locale locale, Map<String, Object> config) {
        GameDto gameDto = (GameDto) config.get(GAME_KEY);
        gameDto.setChallengerId(entity.getFirstPlayerGame().getPlayer().getId());
        gameDto.setOpponentId(entity.getSecondPlayerGame().getPlayer().getId());
        gameDto.setStartingPlayerId(entity.getPlayerTurn().getId());
        gameDto.setGameId(entity.getId());

        return gameDto;
    }

    public Game mapToEntity(GameDto source, Locale locale) {
        throw new UnsupportedOperationException();
    }

    public Game mapToEntity(GameDto source, Locale locale, Map<String, Object> config) {
        throw new UnsupportedOperationException();
    }
}
