package grujic.battleships.web.mapper.custom;


public interface EntityContextMapper <E, S, T> {

    S mapFromEntity(E entity, T context);

    E mapToEntity(S source);
}
