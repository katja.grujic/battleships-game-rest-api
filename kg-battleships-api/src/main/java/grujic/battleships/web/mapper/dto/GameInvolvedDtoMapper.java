package grujic.battleships.web.mapper.dto;

import grujic.battleships.model.Game;
import grujic.battleships.model.PlayerInGameInfo;
import grujic.battleships.web.domain.dto.GameInvolvedDto;
import grujic.battleships.web.mapper.custom.EntityContextMapper;
import grujic.battleships.game.GameStatus;
import org.springframework.stereotype.Component;


@Component
public class GameInvolvedDtoMapper implements EntityContextMapper<Game, GameInvolvedDto, PlayerInGameInfo> {

    @Override
    public GameInvolvedDto mapFromEntity(Game entity, PlayerInGameInfo context) {
        GameInvolvedDto gameInvolvedDto = new GameInvolvedDto();

        gameInvolvedDto.setGameId(entity.getId());
        //mapping first player view
        if (entity.getFirstPlayerGame() == context) {
            gameInvolvedDto.setOpponentId(entity.getSecondPlayerGame().getPlayer().getId());
        } else {
            //mapping second player view
            gameInvolvedDto.setOpponentId(entity.getFirstPlayerGame().getPlayer().getId());
        }

        if (entity.getWon() == null) {
            gameInvolvedDto.setStatus(GameStatus.IN_PROGRESS);
        } else if (entity.getWon().equals(context.getPlayer())) {
            gameInvolvedDto.setStatus(GameStatus.WON);
        } else {
            gameInvolvedDto.setStatus(GameStatus.LOST);
        }

        return gameInvolvedDto;
    }

    @Override
    public Game mapToEntity(GameInvolvedDto source) {
        throw new UnsupportedOperationException();
    }
}
