package grujic.battleships.web.controller;

import grujic.battleships.mapper.BoardMapper;
import grujic.battleships.model.Board;
import grujic.battleships.model.Game;
import grujic.battleships.model.Player;
import grujic.battleships.model.PlayerInGameInfo;
import grujic.battleships.service.GameService;
import grujic.battleships.service.PlayerInGameInfoService;
import grujic.battleships.game.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
public class PlayerControllerIntegrationTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    PlayerController playerController;

    @Autowired
    private GameService gameService;

    @Autowired
    private PlayerInGameInfoService playerInGameInfoService;

    @Autowired
    private BoardMapper boardMapper;

    @Before
    public void setup() throws Exception {
        this.mvc = standaloneSetup(this.playerController).build();// Standalone context
    }

    @Test
    public void handleShotsIntegrationTest() throws Exception {
        Player player = createTestPlayer("alice", "alice@mail.com");
        Player opponent = createTestPlayer("bob", "bob@mail.com");

        GameBoard playerBoard = createTestGameBoard();
        playerBoard.setAliveShips(8);

        GameBoard opponentBoard = createTestGameBoard();

        PlayerInGameInfo playerInfo = createTestPlayerInGameInfo(player, playerBoard);
        PlayerInGameInfo opponentInfo = createTestPlayerInGameInfo(opponent, opponentBoard);

        Game game = createTestGame(playerInfo, opponentInfo);

        String testSalvo = "{\"salvo\":[\"1xA\",\"1xJ\",\"2xA\",\"3xB\",\"5xD\",\"10xA\",\"10xJ\",\"9xG\"]}";

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .put("/player/1/game/1")
                .accept(MediaType.APPLICATION_JSON)
                .content(testSalvo)
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mvc.perform(requestBuilder).andReturn();

        System.out.println(result.getResponse().getContentAsString());
        String expected = "{salvo:" +
                "{1xA:KILL,1xJ:KILL,2xA:MISS,3xB:MISS,5xD:MISS,10xA:KILL,10xJ:HIT,9xG:MISS}," +
                "game:{player_turn:2}}";

        JSONAssert.assertEquals(expected, result.getResponse().getContentAsString(), false);
    }

    private Player createTestPlayer(String name, String email) {
        Player player = new Player(name, email);
        return player;
    }

    private GameBoard createTestGameBoard() {
        GameBoard board = new GameBoard();

        //place ships so the board looks like this
        String[] shipPlacing = new String[]{
                "#..##....#",
                "..........",
                "#.........",
                "#..##....#",
                "#........#",
                "#........#",
                "..........",
                ".........#",
                "..###.....",
                "#.......##"};
        board.setCellsFromString(shipPlacing);
        board.setAliveShips(10);
        board.getCells()[8][9] = new CellWithShip(new Location(), new Ship(ShipType.SUBMARINE));
        board.getCells()[9][9] = new CellWithShip(new Location(), new Ship(ShipType.SUBMARINE));
        return board;
    }

    private PlayerInGameInfo createTestPlayerInGameInfo(Player player, GameBoard gameBoard) {
        Board board = boardMapper.mapToEntity(gameBoard);

        PlayerInGameInfo playerInGameInfo = new PlayerInGameInfo(player, board);
        return playerInGameInfoService.save(playerInGameInfo);
    }

    private Game createTestGame(PlayerInGameInfo playerInfo, PlayerInGameInfo opponentInfo) {
        Game game = new Game(playerInfo, opponentInfo);
        game.setPlayerTurn(playerInfo.getPlayer());
        return gameService.save(game);
    }

}
