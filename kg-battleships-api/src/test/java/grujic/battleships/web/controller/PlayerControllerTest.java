package grujic.battleships.web.controller;

import grujic.battleships.mapper.BoardMapper;
import grujic.battleships.model.Player;
import grujic.battleships.service.BoardService;
import grujic.battleships.service.GameService;
import grujic.battleships.service.PlayerInGameInfoService;
import grujic.battleships.service.PlayerService;
import grujic.battleships.web.converter.LocationToSalvo;
import grujic.battleships.web.converter.SalvoToLocations;
import grujic.battleships.web.domain.dto.PlayerDto;
import grujic.battleships.web.mapper.dto.GameDtoMapper;
import grujic.battleships.web.mapper.dto.GameInvolvedDtoMapper;
import grujic.battleships.web.mapper.dto.PlayerDtoMapper;
import grujic.battleships.web.mapper.form.PlayerFormMapper;
import grujic.battleships.web.service.AutopilotService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@WebMvcTest(value = PlayerController.class)
public class PlayerControllerTest {

    @Autowired
    private MockMvc mvc;
    @MockBean
    PlayerService playerService;
    @MockBean
    GameService gameService;
    @MockBean
    BoardService boardService;
    @MockBean
    PlayerInGameInfoService playerInGameInfoService;
    @MockBean
    AutopilotService autopilotService;
    @MockBean
    PlayerFormMapper playerFormMapper;
    @MockBean
    PlayerDtoMapper playerDtoMapper;
    @MockBean
    GameDtoMapper gameDtoMapper;
    @MockBean
    BoardMapper boardMapper;
    @MockBean
    GameInvolvedDtoMapper gameInvolvedDtoMapper;
    @MockBean
    SalvoToLocations salvoToLocations;
    @MockBean
    LocationToSalvo locationToSalvo;

    String playerJson = "{\"name\":\"JohnDoe\",\"email\":\"john@mail.com\"}";

    @Test
    public void createPlayer() throws Exception {
        //given
        Player mockPlayer = new Player();
        mockPlayer.setId(1L);
        mockPlayer.setName("JohnDoe");
        mockPlayer.setEmail("john@mail.com");

        Mockito.when(playerService.existsByEmail(Mockito.anyString())).thenReturn(false);
        Mockito.when(playerFormMapper.mapToEntity(Mockito.anyObject())).thenReturn(mockPlayer);
        Mockito.when(playerService.save(Mockito.anyObject())).thenReturn(mockPlayer);

        //when
        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post("/player")
                .accept(MediaType.APPLICATION_JSON)
                .content(playerJson)
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mvc.perform(requestBuilder).andReturn();

        //then
        MockHttpServletResponse response = result.getResponse();
        assertEquals(HttpStatus.CREATED.value(), response.getStatus());
        assertEquals("/player/1", response.getHeader(HttpHeaders.LOCATION));
    }

    @Test
    public void getPlayer() throws Exception {
        //given
        Player mockPlayer = new Player();
        mockPlayer.setId(1L);
        mockPlayer.setName("JohnDoe");
        mockPlayer.setEmail("john@mail.com");

        PlayerDto playerDto = new PlayerDto();
        playerDto.setName("JohnDoe");
        playerDto.setEmail("john@mail.com");

        Mockito.when(playerService.existsById(Mockito.anyLong())).thenReturn(true);
        Mockito.when(playerService.getById(Mockito.anyLong())).thenReturn(mockPlayer);
        Mockito.when(playerDtoMapper.mapFromEntity(Mockito.anyObject())).thenReturn(playerDto);

        //when
        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .get("/player/1")
                .accept(MediaType.APPLICATION_JSON);

        MvcResult result = mvc.perform(requestBuilder).andReturn();

        //then
        String expected = "{name:JohnDoe,email:john@mail.com}";
        System.out.println(result.getResponse().getContentAsString());
        JSONAssert.assertEquals(expected, result.getResponse().getContentAsString(), true);

    }
}