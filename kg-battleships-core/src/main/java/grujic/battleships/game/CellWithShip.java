package grujic.battleships.game;

public class CellWithShip extends Cell {

    private Ship ship;

    public CellWithShip(Location location, Ship ship) {
        super(location);
        this.cellStatusSelfView = CellStatus.ALIVE_SHIP;
        this.cellStatusOpponentView = CellStatus.EMPTY;
        this.ship = ship;
    }

    public Ship getShip() {
        return ship;
    }

    public void setShip(Ship ship) {
        this.ship = ship;
    }
}
