package grujic.battleships.game;

import java.util.ArrayList;
import java.util.List;

public class Settings {
    private static final int BATTLESHIP_QUANTITY = 1;
    private static final int DESTROYER_QUANTITY = 2;
    private static final int SUBMARINE_QUANTITY = 3;
    private static final int PATROL_CRAFT_QUANTITY = 4;

    public static final int BOARD_SIZE = 10;

    public static List<Ship> getShips() {
        List<Ship> ships = new ArrayList<>();
        addShips(ships, ShipType.BATTLESHIP, BATTLESHIP_QUANTITY);
        addShips(ships, ShipType.DESTROYER, DESTROYER_QUANTITY);
        addShips(ships, ShipType.SUBMARINE, SUBMARINE_QUANTITY);
        addShips(ships, ShipType.PATROL_CRAFT, PATROL_CRAFT_QUANTITY);

        return ships;
    }

    private static void addShips(List<Ship> ships, ShipType type, int quantity) {
        for (int i = 0; i < quantity; i++) {
            ships.add(new Ship(type));
        }
    }
}
