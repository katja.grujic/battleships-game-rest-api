package grujic.battleships.game;

public enum ShotStatus {
    HIT,
    KILL,
    MISS;
}
