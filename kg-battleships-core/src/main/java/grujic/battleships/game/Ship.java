package grujic.battleships.game;

import java.io.Serializable;

public class Ship implements Serializable {
    private ShipType shipType;
    private int shots;

    public Ship(ShipType shipType) {
        this.shipType = shipType;
        this.shots = 0;
    }

    public ShipType getShipType() {
        return shipType;
    }

    public void setShipType(ShipType shipType) {
        this.shipType = shipType;
    }

    public int getShots() {
        return shots;
    }

    public void setShots(int shots) {
        this.shots = shots;
    }

    public void addShot() {
        this.shots++;
    }

    public boolean isAlive(){
        return this.shots < this.shipType.getLength();
    }
}
