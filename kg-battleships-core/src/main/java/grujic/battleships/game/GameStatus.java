package grujic.battleships.game;

public enum GameStatus {
    WON,
    LOST,
    IN_PROGRESS;
}
