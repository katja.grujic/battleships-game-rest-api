package grujic.battleships.game;

public enum ShipType {
    BATTLESHIP(4),
    DESTROYER(3),
    SUBMARINE(2),
    PATROL_CRAFT(1);

    private int length;

    ShipType(int length) {
        this.length = length;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }
}
