package grujic.battleships.game;

import java.util.List;
import java.util.Random;

public class GameBoard {

    private Cell[][] cells;

    private int aliveShips;

    private static Random rnd = new Random(System.currentTimeMillis());

    public GameBoard() {
    }

    public GameBoard(Cell[][] cells) {
        this.cells = cells;
        this.aliveShips = Settings.getShips().size();
    }

    public Cell[][] getCells() {
        return cells;
    }

    public void setCells(Cell[][] cells) {
        this.cells = cells;
    }

    public int getAliveShips() {
        return aliveShips;
    }

    public void setAliveShips(int aliveShips) {
        this.aliveShips = aliveShips;
    }

    public static BoardBuilder builder() {
        return new BoardBuilder();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < Settings.BOARD_SIZE; i++) {
            for (int j = 0; j < Settings.BOARD_SIZE; j++) {
                sb.append(cells[i][j].cellStatusSelfView);
            }
            sb.append("\n");
        }
        return sb.toString();
    }

    public String[] toStringSelfView() {
        String[] selfView = new String[Settings.BOARD_SIZE];
        for (int i = 0; i < Settings.BOARD_SIZE; i++) {
            StringBuilder sb = new StringBuilder();
            for (int j = 0; j < Settings.BOARD_SIZE; j++) {
                sb.append(cells[i][j].cellStatusSelfView);
            }
            selfView[i] = sb.toString();
        }
        return selfView;
    }

    public String[] toStringOpponentView() {
        String[] opponentView = new String[Settings.BOARD_SIZE];
        for (int i = 0; i < Settings.BOARD_SIZE; i++) {
            StringBuilder sb = new StringBuilder();
            for (int j = 0; j < Settings.BOARD_SIZE; j++) {
                sb.append(cells[i][j].cellStatusOpponentView);
            }
            opponentView[i] = sb.toString();
        }
        return opponentView;
    }

    public ShotStatus getShotResult(Location location) {
        Cell cell = this.cells[location.getRow()][location.getColumn()];

        //no ship at that cell
        if (!(cell instanceof CellWithShip)) {
            cell.cellStatusSelfView = CellStatus.MISS;
            cell.cellStatusOpponentView = CellStatus.MISS;
            return ShotStatus.MISS;
        } else {
            //already shot at that location
            if (cell.cellStatusOpponentView != CellStatus.EMPTY) {
                return ShotStatus.MISS;
            }

            //hit a ship
            cell.cellStatusSelfView = CellStatus.HIT;
            cell.cellStatusOpponentView = CellStatus.HIT;
            ((CellWithShip) cell).getShip().addShot();

            //hit and kill
            if (!(((CellWithShip) cell).getShip().isAlive())) {
                this.aliveShips--;
                return ShotStatus.KILL;
            }

            return ShotStatus.HIT;
        }

    }

    public void setCellsFromString(String[] strings) {
        this.cells = new Cell[Settings.BOARD_SIZE][Settings.BOARD_SIZE];
        int column = 0;
        int row = 0;
        for (String string : strings) {
            row++;
            column = 0;
            for (int index = 0; index < string.length(); index++) {
                column++;
                cells[row - 1][column - 1] = new Cell();
                if (Character.toString(string.charAt(index)).equals("#")) {
                    cells[row - 1][column - 1] = new CellWithShip(new Location(), new Ship(ShipType.PATROL_CRAFT));
                } else {
                    cells[row - 1][column - 1].cellStatusSelfView = CellStatus.EMPTY;
                }
                cells[row - 1][column - 1].cellStatusOpponentView = CellStatus.EMPTY;
            }
        }
    }

    public static class BoardBuilder {

        private Cell[][] cells;

        public BoardBuilder() {
        }

        public GameBoard build() {
            return new GameBoard(cells);
        }

        public BoardBuilder setShips(List<Ship> ships) {
            this.cells = new Cell[Settings.BOARD_SIZE][Settings.BOARD_SIZE];
            init();

            for (Ship ship : ships) {
                boolean placed = false;
                do {
                    placed = placeShip(ship);
                } while (!placed);

            }

            return this;
        }

        private void init() {
            for (int i = 0; i < Settings.BOARD_SIZE; i++) {
                for (int j = 0; j < Settings.BOARD_SIZE; j++) {
                    cells[i][j] = new Cell();
                    cells[i][j].cellStatusSelfView = CellStatus.EMPTY;
                    cells[i][j].cellStatusOpponentView = CellStatus.EMPTY;
                }
            }
        }

        private boolean placeShip(Ship ship) {
            int orientation = (rnd.nextBoolean() ? Orientation.VERTICAL : Orientation.HORIZONTAL);
            Location location;

            // choosing starting location so ship can fit on board in all its length
            if (orientation == Orientation.HORIZONTAL) {
                location = new Location(rnd.nextInt(Settings.BOARD_SIZE),
                        rnd.nextInt(Settings.BOARD_SIZE - ship.getShipType().getLength() + 1));
            } else {
                location = new Location(rnd.nextInt(Settings.BOARD_SIZE - ship.getShipType().getLength() + 1),
                        rnd.nextInt(Settings.BOARD_SIZE));
            }

            if (!isPossibleToPlaceShip(ship, location, orientation)) {
                return false;
            }

            placeShipOnLocation(ship, location, orientation);
            return true;
        }

        private void placeShipOnLocation(Ship ship, Location location, int orientation) {
            if (orientation == Orientation.HORIZONTAL) {
                placeShipHorizontally(ship, location);
            } else {
                placeShipVertically(ship, location);
            }
        }

        private void placeShipHorizontally(Ship ship, Location location) {
            int len = ship.getShipType().getLength();
            for (int i = location.getColumn(); i < location.getColumn() + len; i++) {
                cells[i][location.getRow()] = new CellWithShip(new Location(location.getRow(), i), ship);
            }
        }


        private void placeShipVertically(Ship ship, Location location) {
            int len = ship.getShipType().getLength();
            for (int i = location.getRow(); i < location.getRow() + len; i++) {
                cells[location.getColumn()][i] = new CellWithShip(new Location(i, location.getColumn()), ship);
            }

        }

        private boolean isPossibleToPlaceShip(Ship ship, Location location, int orientation) {
            if (orientation == Orientation.HORIZONTAL) {
                return isPossibleToPlaceShipHorizontally(ship, location);
            } else {
                return isPossibleToPlaceShipVertically(ship, location);
            }
        }

        private boolean isPossibleToPlaceShipHorizontally(Ship ship, Location location) {
            if (isShipAreaEmptyHorizontally(ship, location)) {
                return isNeighbourAreaEmptyHorizontally(ship, location);
            }
            return false;
        }

        private boolean isShipAreaEmptyHorizontally(Ship ship, Location location) {
            boolean isEmpty = true;
            for (int i = location.getColumn(); i < location.getColumn() + ship.getShipType().getLength(); i++) {
                if (cells[i][location.getRow()].cellStatusSelfView == CellStatus.ALIVE_SHIP) {
                    isEmpty = false;
                    break;
                }
            }
            return isEmpty;
        }

        private boolean isNeighbourAreaEmptyHorizontally(Ship ship, Location location) {
            int x = location.getColumn();
            int y = location.getRow();
            int len = ship.getShipType().getLength();

            // check left side of the ship
            if (x > 0) {
                if (cells[x - 1][y].cellStatusSelfView == CellStatus.ALIVE_SHIP) {
                    return false;
                }
            }

            // check right side of the ship
            if ((x + len - 1) < (Settings.BOARD_SIZE - 1)) {
                if (cells[x + len][y].cellStatusSelfView == CellStatus.ALIVE_SHIP) {
                    return false;
                }
            }

            //check above the ship
            if (y > 0) {
                for (int i = x; i < (x + len); i++) {
                    if (cells[i][y - 1].cellStatusSelfView == CellStatus.ALIVE_SHIP) {
                        return false;
                    }
                }
            }

            //check under the ship
            if (y < Settings.BOARD_SIZE - 1) {
                for (int i = x; i < x + len; i++) {
                    if (cells[i][y + 1].cellStatusSelfView == CellStatus.ALIVE_SHIP) {
                        return false;
                    }
                }
            }

            return true;
        }


        private boolean isPossibleToPlaceShipVertically(Ship ship, Location location) {
            if (isShipAreaEmptyVertically(ship, location)) {
                return isNeighbourAreaEmptyVertically(ship, location);
            }
            return false;
        }

        private boolean isShipAreaEmptyVertically(Ship ship, Location location) {
            boolean isEmpty = true;
            for (int i = location.getRow(); i < location.getRow() + ship.getShipType().getLength(); i++) {
                if (cells[location.getColumn()][i].cellStatusSelfView == CellStatus.ALIVE_SHIP) {
                    isEmpty = false;
                    break;
                }
            }
            return isEmpty;
        }

        private boolean isNeighbourAreaEmptyVertically(Ship ship, Location location) {
            int x = location.getColumn();
            int y = location.getRow();
            int len = ship.getShipType().getLength();

            //check above the ship
            if (y > 0) {
                if (cells[x][y - 1].cellStatusSelfView == CellStatus.ALIVE_SHIP) {
                    return false;
                }
            }

            //check under the ship
            if ((y + len - 1) < Settings.BOARD_SIZE - 1) {
                if (cells[x][y + len].cellStatusSelfView == CellStatus.ALIVE_SHIP) {
                    return false;
                }
            }

            //check left side of the ship
            if (x > 0) {
                for (int i = y; i < y + len; i++) {
                    if (cells[x - 1][i].cellStatusSelfView == CellStatus.ALIVE_SHIP) {
                        return false;
                    }
                }
            }

            //check right side of the ship
            if (x < Settings.BOARD_SIZE - 1) {
                for (int i = y; i < y + len; i++) {
                    if (cells[x + 1][i].cellStatusSelfView == CellStatus.ALIVE_SHIP) {
                        return false;
                    }
                }
            }

            return true;
        }
    }
}