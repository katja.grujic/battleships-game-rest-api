package grujic.battleships.game;

import java.io.Serializable;

public class Cell implements Serializable {

    private Location location;

    protected CellStatus cellStatusSelfView;

    protected CellStatus cellStatusOpponentView;

    public Cell() {
    }

    public Cell(Location location) {
        this.location = location;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public CellStatus getCellStatusSelfView() {
        return cellStatusSelfView;
    }

    public void setCellStatusSelfView(CellStatus cellStatusSelfView) {
        this.cellStatusSelfView = cellStatusSelfView;
    }

    public CellStatus getCellStatusOpponentView() {
        return cellStatusOpponentView;
    }

    public void setCellStatusOpponentView(CellStatus cellStatusOpponentView) {
        this.cellStatusOpponentView = cellStatusOpponentView;
    }
}
