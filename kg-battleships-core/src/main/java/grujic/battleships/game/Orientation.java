package grujic.battleships.game;

public abstract class Orientation {
    public static final int HORIZONTAL = 0;
    public static final int VERTICAL = 1;

    private Orientation() {

    }
}
