package grujic.battleships.game;

public enum CellStatus {
    EMPTY("."),
    ALIVE_SHIP("#"),
    HIT("X"),
    MISS("O");

    private String label;

    CellStatus(String label) {
        this.label = label;
    }

    @Override
    public String toString() {
        return label;
    }
}
