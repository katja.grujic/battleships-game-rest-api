package grujic.battleships.service.impl;

import grujic.battleships.model.Board;
import grujic.battleships.repository.BoardRepository;
import grujic.battleships.service.BoardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class BoardServiceImpl implements BoardService {

    @Autowired
    private BoardRepository boardRepository;

    @Override
    @Transactional
    public Board save(Board board) {
        return boardRepository.saveAndFlush(board);
    }
}
