package grujic.battleships.service.impl;

import grujic.battleships.exception.PlayerNotInGameException;
import grujic.battleships.model.Game;
import grujic.battleships.model.Player;
import grujic.battleships.model.PlayerInGameInfo;
import grujic.battleships.repository.PlayerInGameInfoRepository;
import grujic.battleships.service.PlayerInGameInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PlayerInGameInfoServiceImpl implements PlayerInGameInfoService {

    @Autowired
    PlayerInGameInfoRepository playerInGameInfoRepository;

    @Override
    public PlayerInGameInfo save(PlayerInGameInfo playerInGameInfo) {
        return playerInGameInfoRepository.saveAndFlush(playerInGameInfo);
    }

    @Override
    public PlayerInGameInfo getSelfInfo(Game game, Player player) {
        if (game.getFirstPlayerGame().getPlayer().getId().equals(player.getId())) {
            return game.getFirstPlayerGame();
        } else if (game.getSecondPlayerGame().getPlayer().getId().equals(player.getId())) {
            return game.getSecondPlayerGame();
        }

        // if the player is not participating in the game
        throw new PlayerNotInGameException(player.getId(), game.getId());
    }

    @Override
    public PlayerInGameInfo getOpponentInfo(Game game, Player player) {
        if (game.getFirstPlayerGame().getPlayer().getId().equals(player.getId())) {
            return game.getSecondPlayerGame();
        } else {
            return game.getFirstPlayerGame();
        }

    }

    @Override
    public List<Long> findAllByPlayerId(Long playerId) {
         return playerInGameInfoRepository.finalAllIdsByPlayerId(playerId);
    }


}
