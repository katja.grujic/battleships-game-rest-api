package grujic.battleships.service.impl;

import grujic.battleships.exception.GameNotFoundException;
import grujic.battleships.model.Game;
import grujic.battleships.repository.GameRepository;
import grujic.battleships.service.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class GameServiceImpl implements GameService {

    @Autowired
    private GameRepository gameRepository;

    @Override
    public Game save(Game game) {
        return gameRepository.saveAndFlush(game);
    }

    @Override
    public Game getById(Long id) {
        return gameRepository.findById(id).
                orElseThrow(() -> new GameNotFoundException(id));
    }

    @Override
    @Transactional
    public Game findByPlayerInGameInfo(Long infoId) {
        return gameRepository.findGameByPlayerViewId(infoId);
    }
}
