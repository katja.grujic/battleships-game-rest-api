package grujic.battleships.service;

import grujic.battleships.model.Board;

public interface BoardService {

    Board save(Board board);
}
