package grujic.battleships.service;

import grujic.battleships.model.Player;

import java.util.List;

public interface PlayerService {
    Player save(Player player);
    boolean existsByEmail(String email);
    boolean existsById(Long id);
    Player getById(Long id);
    List<Player> getAll();
}
