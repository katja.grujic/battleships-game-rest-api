package grujic.battleships.service;

import grujic.battleships.model.Game;
import grujic.battleships.model.Player;
import grujic.battleships.model.PlayerInGameInfo;

import java.util.List;

public interface PlayerInGameInfoService {

    PlayerInGameInfo save(PlayerInGameInfo playerInGameInfo);

    PlayerInGameInfo getSelfInfo(Game game, Player player);

    PlayerInGameInfo getOpponentInfo(Game game, Player player);

    List<Long> findAllByPlayerId(Long playerId);
}
