package grujic.battleships.service;

import grujic.battleships.model.Game;

public interface GameService {

    Game save(Game game);

    Game getById(Long id);

    Game findByPlayerInGameInfo(Long infoId);
}
