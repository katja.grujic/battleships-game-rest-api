package grujic.battleships.exception;

public class EmailAlreadyExistsException extends RuntimeException{
    private String email;

    public  EmailAlreadyExistsException (String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
