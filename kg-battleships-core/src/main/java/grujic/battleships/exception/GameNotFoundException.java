package grujic.battleships.exception;

public class GameNotFoundException extends RuntimeException {

    private Long gameId;

    public GameNotFoundException(Long gameId) {
        this.gameId = gameId;
    }

    public Long getGameId() {
        return gameId;
    }

    public void setGameId(Long gameId) {
        this.gameId = gameId;
    }
}
