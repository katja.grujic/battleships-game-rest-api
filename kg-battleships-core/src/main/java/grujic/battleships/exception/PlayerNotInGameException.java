package grujic.battleships.exception;

public class PlayerNotInGameException extends RuntimeException {

    private Long playerId;

    private Long gameId;

    public PlayerNotInGameException(Long playerId, Long gameId) {
        this.playerId = playerId;
        this.gameId = gameId;
    }

    public Long getPlayerId() {
        return playerId;
    }

    public Long getGameId() {
        return gameId;
    }

}
