package grujic.battleships.exception;

public class PlayerNotFoundException extends RuntimeException {

    private Long playerId;

    public PlayerNotFoundException(Long playerId) {
        this.playerId = playerId;
    }

    public Long getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Long playerId) {
        this.playerId = playerId;
    }
}
