package grujic.battleships.mapper;

import grujic.battleships.game.Cell;
import grujic.battleships.game.GameBoard;
import grujic.battleships.model.Board;
import org.springframework.stereotype.Component;

import java.io.*;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

@Component
public class BoardMapper {
    private static final String GAME_BOARD_KEY = "gameBoardKey";
    private static final String BOARD_KEY = "boardKey";

    public GameBoard mapFromEntity(Board entity) {
        return mapFromEntity(entity, null);
    }

    public Board mapToEntity(GameBoard source) {
        return mapToEntity(source, null);
    }

    public GameBoard mapFromEntity(Board entity, Locale locale) {
        Map<String, Object> config = new HashMap<>();
        config.put(GAME_BOARD_KEY, new GameBoard());

        return mapFromEntity(entity, locale, config);
    }

    public GameBoard mapFromEntity(Board entity, Locale locale, Map<String, Object> config) {
        GameBoard board = (GameBoard) config.get(GAME_BOARD_KEY);

        Cell[][] cells = null;
        try (
                ByteArrayInputStream bis = new ByteArrayInputStream(entity.getCells());
                ObjectInputStream ois = new ObjectInputStream(bis)
        ) {
            cells = (Cell[][]) ois.readObject();
        } catch(IOException ignorable) {
        } catch (ClassNotFoundException ignorable) {
        }

        board.setCells(cells);
        board.setAliveShips(entity.getAliveShips());
        return board;
    }

    public Board mapToEntity(GameBoard source, Locale locale) {
        Map<String, Object> config = new HashMap<>();
        config.put(BOARD_KEY, new Board());

        return mapToEntity(source, locale, config);
    }

    public Board mapToEntity(GameBoard source, Locale locale, Map<String, Object> config) {
        Board board = (Board) config.get(BOARD_KEY);

        byte[] bytes = null;
        try (
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                ObjectOutputStream oos = new ObjectOutputStream(bos)
        ) {
            oos.writeObject(source.getCells());
            oos.flush();
            bytes = bos.toByteArray();
        } catch(IOException ignorable) {
            System.out.println(ignorable);
        }

        board.setCells(bytes);
        board.setAliveShips(source.getAliveShips());
        return board;
    }
}