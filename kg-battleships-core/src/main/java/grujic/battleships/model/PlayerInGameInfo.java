package grujic.battleships.model;

import javax.persistence.*;

@Entity
public class PlayerInGameInfo {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "player_id")
    private Player player;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "board_id")
    private Board selfBoard;

    private boolean autopilot;

    public PlayerInGameInfo() {
    }

    public PlayerInGameInfo(Player player, Board selfBoard) {
        this.player = player;
        this.selfBoard = selfBoard;
        this.autopilot = false;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public Board getSelfBoard() {
        return selfBoard;
    }

    public void setSelfBoard(Board selfBoard) {
        this.selfBoard = selfBoard;
    }

    public boolean isAutopilot() {
        return autopilot;
    }

    public void setAutopilot(boolean autopilot) {
        this.autopilot = autopilot;
    }

}

