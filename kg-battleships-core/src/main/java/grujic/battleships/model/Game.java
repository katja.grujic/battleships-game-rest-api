package grujic.battleships.model;

import javax.persistence.*;

@Entity
public class Game {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @OneToOne
    @JoinColumn(name = "player_info1")
    private PlayerInGameInfo firstPlayerGame;

    @OneToOne
    @JoinColumn(name = "player_info2")
    private PlayerInGameInfo secondPlayerGame;

    @ManyToOne
    @JoinColumn(name = "player_turn")
    private Player playerTurn;

    @ManyToOne
    @JoinColumn(name = "won")
    private Player won;

    public Game() {
    }

    public Game(PlayerInGameInfo firstPlayerGame, PlayerInGameInfo secondPlayerGame) {
        this.firstPlayerGame = firstPlayerGame;
        this.secondPlayerGame = secondPlayerGame;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PlayerInGameInfo getFirstPlayerGame() {
        return firstPlayerGame;
    }

    public void setFirstPlayerGame(PlayerInGameInfo firstPlayerGame) {
        this.firstPlayerGame = firstPlayerGame;
    }

    public PlayerInGameInfo getSecondPlayerGame() {
        return secondPlayerGame;
    }

    public void setSecondPlayerGame(PlayerInGameInfo secondPlayerGame) {
        this.secondPlayerGame = secondPlayerGame;
    }

    public Player getPlayerTurn() {
        return playerTurn;
    }

    public void setPlayerTurn(Player playerTurn) {
        this.playerTurn = playerTurn;
    }

    public Player getWon() {
        return won;
    }

    public void setWon(Player won) {
        this.won = won;
    }
}