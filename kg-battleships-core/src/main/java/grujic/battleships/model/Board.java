package grujic.battleships.model;

import javax.persistence.*;

@Entity
public class Board {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Lob
    @Column(name = "cells")
    private byte[] cells;

    private int aliveShips;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public byte[] getCells() {
        return cells;
    }

    public void setCells(byte[] cells) {
        this.cells = cells;
    }

    public int getAliveShips() {
        return aliveShips;
    }

    public void setAliveShips(int aliveShips) {
        this.aliveShips = aliveShips;
    }
}
