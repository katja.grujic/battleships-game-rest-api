package grujic.battleships.repository;

import grujic.battleships.model.PlayerInGameInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PlayerInGameInfoRepository extends JpaRepository<PlayerInGameInfo, Long> {
    @Query("SELECT p.id " +
            "FROM PlayerInGameInfo p " +
            "WHERE p.player.id=:playerId")
    List<Long> finalAllIdsByPlayerId(@Param("playerId") Long playerId);
}
