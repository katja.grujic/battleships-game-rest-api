package grujic.battleships.repository;

import grujic.battleships.model.Game;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface GameRepository extends JpaRepository<Game, Long> {

    @Query("SELECT g " +
            "FROM Game g " +
            "WHERE g.firstPlayerGame.id=:infoId " +
            "OR g.secondPlayerGame.id=:infoId")
    Game findGameByPlayerViewId(@Param("infoId") Long infoId);
}